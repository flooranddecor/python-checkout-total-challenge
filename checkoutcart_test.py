import unittest
from checkoutcart import build_checkout_cart_with_default_rules

def price_check(products):
    """Returns the total after scanning each productCode in productCodes.
    (no need to change anything here)

    @param productCodes list of productCodes to scan.
    @return the CheckoutCart total
    """
    checkout_cart = build_checkout_cart_with_default_rules()

    for product in products:
        checkout_cart.scan(product)

    return checkout_cart.get_total()


class TestCheckoutCart(unittest.TestCase):

    def test_incremental_scanning(self):
        checkout_cart = build_checkout_cart_with_default_rules()
        self.assertEqual(checkout_cart.get_total(), 0)

        checkout_cart.scan('A')
        self.assertEqual(checkout_cart.get_total(), 50)

        checkout_cart.scan('B')
        self.assertEqual(checkout_cart.get_total(), 80)

        checkout_cart.scan('A')
        self.assertEqual(checkout_cart.get_total(), 130)

        checkout_cart.scan('A')
        self.assertEqual(checkout_cart.get_total(), 160)

        checkout_cart.scan('B')
        self.assertEqual(checkout_cart.get_total(), 175)


    def test_price_check_scenarios(self):
        self.assertEqual(price_check([]), 0)
        self.assertEqual(price_check(['']), 0)
        self.assertEqual(price_check(['A']), 50)
        self.assertEqual(price_check(['A', 'B']), 80)
        self.assertEqual(price_check(['C', 'D', 'B', 'A']), 115)

        self.assertEqual(price_check(['A', 'A']), 100)
        self.assertEqual(price_check(['A', 'A', 'A']), 130)
        self.assertEqual(price_check(['A', 'A', 'A', 'A']), 180)
        self.assertEqual(price_check(['A', 'A', 'A', 'A', 'A']), 230)
        self.assertEqual(price_check(['A', 'A', 'A', 'A', 'A', 'A']), 260)

        self.assertEqual(price_check(['A', 'A', 'A', 'B']), 160)
        self.assertEqual(price_check(['A', 'A', 'A', 'B', 'B']), 175)
        self.assertEqual(price_check(['A', 'A', 'A', 'B', 'B', 'D']), 190)
        self.assertEqual(price_check(['D', 'A', 'B', 'A', 'B', 'A']), 190)

