class CheckoutCart:
    """
        TODO: Determine desired data structure for rules member variable.
            Must store what is in the product pricing table
    """

    def __init__(self, rules: dict):
        self.rules = rules

    def scan(self, product_code: str) -> None:
        """
        TODO: Implement scan
        """

    def get_total(self) -> int:
        """
        TODO: Implement get_total
        """
        return 0


def build_checkout_cart_with_default_rules() -> int:
    """
        TODO: Implement Rules Data structure with data from pricing table

            Item   Unit     Special
                   Price    Price
            --------------------------
            A      50       3 for 130
            B      30       2 for 45
            C      20
            D      15
    """

    default_rules = {}
    
    return CheckoutCart(default_rules)